class XHR {
    constructor() {
        this.xhr = this.getXmlHttpRequestObject();
        this.callbacks = {};
    }

    registerCallback(endpoint, callback) {
        this.callbacks[endpoint] = callback;
    }

    xhrCallback() {
        // Check response is ready or not
        if (this.xhr.readyState == 4 && this.xhr.status == 200) {
            var message = JSON.parse(this.xhr.responseText);

            // Call the callback function
            if (message["type"] in this.callbacks) {
                this.callbacks[message["type"]](message["data"]);
            } else {
                console.log("No callback for type: " + message["type"]);
            }
        }
    }

    getXmlHttpRequestObject() {
        if (!this.xhr) {
            // Create a new XMLHttpRequest object 
            this.xhr = new XMLHttpRequest();
            let boundFunction = (function() { 
                this.xhrCallback();      
            }).bind(this);
            this.xhr.onreadystatechange = boundFunction;
        }

        return this.xhr;
    };

    // Send a POST request to the givne server endpoint. The data is sent as JSON (no need to stringify)
    sendPostRequest(endpoint, data) {
        // asynchronous requests
        this.xhr.open("POST", "http://127.0.0.1:8000/" + endpoint, true);
        this.xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        var res = {};
        if ("username" in localStorage && "uuid" in localStorage) {
            res["username"] = localStorage.getItem("username");
            res["uuid"] = localStorage.getItem("uuid");
        } else {
            res["username"] = "guest";
            res["uuid"] = "guest";
        }
        res["data"] = data;

        // Send the request over the network
        this.xhr.send(JSON.stringify(res));
    }
}

var xhr = new XHR();
export default xhr;