import XHR from "./xhr.js";

class Register {
	constructor(form, fields) {
		this.form = form;
		this.fields = fields;
		this.validateonSubmit();

		XHR.registerCallback("registerResult", (data) => {
			if (data["success"] == true) {
				localStorage.setItem("auth", 1);
				localStorage.setItem("uuid", data["uuid"]);
				localStorage.setItem("username", data["username"]);
				window.location.replace("/dashboard");
			} else {
				this.setStatus(
					document.querySelector("#username"),
					"username already in use or invalid",
					"error"
				);
			}
		});
	}

	validateonSubmit() {
		let self = this;

		this.form.addEventListener("submit", (e) => {
			e.preventDefault();
			var error = 0;
			var res = {};

			// Verify all fields
			self.fields.forEach((field) => {
				const input = document.querySelector(`#${field}`);
				if (self.validateFields(input) == false) {
					error++;
				}
				res[field] = input.value;
			});

			// Send all field data to the backend
			if (error == 0) {
				XHR.sendPostRequest("register", res);
			}
		});
	}

	validateFields(field) {
		if (field.value.trim() === "") {
			this.setStatus(
				field,
				`${field.previousElementSibling.innerText} cannot be blank`,
				"error"
			);
			return false;
		} else {
			if (field.type == "password") {
				if (field.value.length < 5) {
					this.setStatus(
						field,
						`${field.previousElementSibling.innerText} must be at least 5 characters`,
						"error"
					);
					return false;
				} else {
					this.setStatus(field, null, "success");
					return true;
				}
			} else {
				this.setStatus(field, null, "success");
				return true;
			}
		}
	}

	setStatus(field, message, status) {
		const errorMessage = field.parentElement.querySelector(".error-message");

		if (status == "success") {
			if (errorMessage) {
				errorMessage.innerText = "";
			}
			field.classList.remove("input-error");
		}

		if (status == "error") {
			errorMessage.innerText = message;
			field.classList.add("input-error");
		}
	}
}

const form = document.querySelector(".registerForm");
if (form) {
	const fields = ["username", "password"];
	const validator = new Register(form, fields);
}
