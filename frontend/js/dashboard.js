import XHR from "./xhr.js";

var selectedFilters = [];
var filterCount = 0;
var currentSearchResults = [];
var currentLiked = {};
var currentDisliked = {};
var currentTab = "searchTab";
var initPrefs = true;

// Filters

function createNewFilterField() {
    // Get template
    var template = document.getElementById("filterTemplate");
    var clone = template.content.cloneNode(true);

    // Set id
    clone.firstElementChild.id = "filter_" + filterCount;
    filterCount++;

    // Get parent
    var parent = document.getElementById("filterList");

    // Append clone to parent
    parent.appendChild(clone);
}

function populateFilters(id = null) {
    if (id == null) {
        // Get all dropdowns
        var dropdowns = document.getElementsByClassName("dropdown-content");
    } else {
        // Get dropdown with id
        var dropdowns = document.getElementById("filter_" + id).getElementsByClassName("dropdown-content");
    }

    Array.from(dropdowns).forEach(dropdown => {
        dropdown.innerHTML = "";
        for (var i = 0; i < selectedFilters.length; i++) {
            var a = document.createElement("a");
            a.textContent = selectedFilters[i];
            a.onclick = function() {
                // Get parent
                var filters = document.getElementById("filterList");
                var currentFilter = this.parentNode.parentNode.parentNode;

                // Get parent
                var dropdown = this.parentNode.parentNode;
                
                // Get button from parent
                var button = dropdown.getElementsByClassName("dropbtn")[0];

                // Add current filter back to list
                if (button.textContent != "Add Filter") {
                    selectedFilters.push(button.textContent);
                }

                // Set button text to selected filter
                button.textContent = this.textContent;

                // Make filter visible
                currentFilter.getElementsByClassName("filterElemA")[0].style.visibility = "visible";
                currentFilter.getElementsByClassName("filterElemIn")[0].style.visibility = "visible";

                // If this is the latest filter, create a new one
                if (currentFilter.id == "filter_" + (filterCount - 1)) {
                    createNewFilterField();
                }

                // Remove filter from list
                var index = selectedFilters.indexOf(this.textContent);
                if (index > -1) {
                    selectedFilters.splice(index, 1);
                }

                populateFilters();
            }
            
            dropdown.appendChild(a);
        }
    });
}

function getFiltersPOST() {
     // Get all checked checkboxes and store their values in an array
    var checked = [];
    var checkboxes = document.querySelectorAll('.typeSelector:checked');
    for (var i = 0; i < checkboxes.length; i++) {
        checked.push(checkboxes[i].value);
    }

    XHR.sendPostRequest("filters", {"checkedFilters": checked});
}

XHR.registerCallback("filters", function(data) {
    // Fill the dropdown with the received filters
    selectedFilters = data;
    
    populateFilters();

    if (initPrefs) {
        initPrefs = false;
        getLikedDisliked(); // Get user's liked / disliked from server
    }
});

// Returns the user selected filters and their values
function getUserFilters() {
    var chosenFilters = document.getElementsByClassName("dropbtn");
    var chosenValues = document.getElementsByClassName("filterElemIn");
    var res = {};

    for (var i = 0; i < chosenFilters.length; i++) {
        if (chosenValues[i].value != "") {
            res[chosenFilters[i].textContent] = chosenValues[i].value;
        }
    }

    for (var unused in selectedFilters) {
        if (!(selectedFilters[unused] in res)) res[selectedFilters[unused]] = "";
    }

    return res;
}

// Search

function sendSearchPOST() {
    // Get all checked checkboxes and store their values in an array
    var checked = [];
    var checkboxes = document.querySelectorAll('.typeSelector:checked');
    for (var i = 0; i < checkboxes.length; i++) {
        checked.push(checkboxes[i].value);
    }

    var data = {"checkedFilters": checked, "userFilters": getUserFilters()};

    // Disable search button
    document.getElementById("searchButton").disabled = true;

    // Show loading animation
    document.getElementById("loadingSearch").classList.add("visible");

    XHR.sendPostRequest("search", data);
}

function fillInSearchResults() {
    clearResultsList();

    // Fill the results with the received results
    for (const [key, value] of Object.entries(currentSearchResults)) {
        createNewResultCard(value["thumbLink"], value["title"], value["link"], key, value["type"]);
    }
}

XHR.registerCallback("searchResults", function(data) {
    // Enable search button
    document.getElementById("searchButton").disabled = false;

    // Hide loading animation
    document.getElementById("loadingSearch").classList.remove("visible");

    if (!data["success"]) {
        document.getElementById("noResultsMessage").hidden = false;
        return;
    } else {
        document.getElementById("noResultsMessage").hidden = true;
        currentSearchResults = data["results"];
        if (currentTab == "searchTab") {
            fillInSearchResults();
        }
    }
});

function createNewResultCard(thumbLink, title, link, id, type) {
    // Get template
    var template = document.getElementById("resultTemplate");
    var clone = template.content.cloneNode(true);

    // Set id
    clone.firstElementChild.id = "searchResult_" + id;

    if (thumbLink != null) {
        // Set thumbnail
        clone.firstElementChild.getElementsByClassName("thumbnailImg")[0].src = thumbLink;

        // Set thumbnail on click
        clone.firstElementChild.getElementsByClassName("thumbnailImg")[0].onclick = function() {
            window.open(link, "_blank");
        }
    }

    // Set title
    clone.firstElementChild.getElementsByClassName("resultCardTitle")[0].textContent = title;

    // Set title on click
    clone.firstElementChild.getElementsByClassName("resultCardTitle")[0].onclick = function() {
        window.open(link, "_blank");
    }

    // Toggle thumb colors
    toggleThumbs(id, clone);

    // Set thumbs up on click
    clone.firstElementChild.getElementsByClassName("fa-thumbs-up")[0].onclick = function() {
        // Get parent
        var parent = this.parentNode.parentNode.parentNode;

        // Get id
        var id = parent.id.split("_")[1];

        // test if currently liked
        if (id in currentLiked) {
            XHR.sendPostRequest("removeThumbsUp", {"id": id});
        } else {
            // Send POST
            XHR.sendPostRequest("thumbsUp", {"id": id, "firstUnset": (id in currentLiked),
                "properties": {
                "title": title, "link": link, "thumbnail": thumbLink, "type": type
            }});
        }
    }

    // Set thumbs down on click
    clone.firstElementChild.getElementsByClassName("fa-thumbs-down")[0].onclick = function() {
        // Get parent
        var parent = this.parentNode.parentNode.parentNode;

        // Get id
        var id = parent.id.split("_")[1];

        // test if currently disliked
        if (id in currentDisliked) {
            XHR.sendPostRequest("removeThumbsDown", {"id": id});
        } else {
            // Send POST
            XHR.sendPostRequest("thumbsDown", {"id": id, "firstUnset": (id in currentLiked),
                "properties": {
                "title": title, "link": link, "thumbnail": thumbLink, "type": type
            }});
        }
    }

    // Get parent
    var parent = document.getElementById("resultsList");

    // Append clone to parent
    parent.appendChild(clone);
}

// Feedback

XHR.registerCallback("feedbackResult", function(data) {
    if (data["success"]) {
        var res = data["properties"];
        res["id"] = data["id"];

        if (data["preference"] == true) {
            currentLiked[res["id"]] = res;
            if (res["id"] in currentDisliked) delete currentDisliked[res["id"]];
        } else {
            currentDisliked[res["id"]] = res;
            if (res["id"] in currentLiked) delete currentLiked[res["id"]];
        }

        toggleThumbs(data["id"], null);
    }
});

function toggleThumbs(id, elem) {
    if (elem == null) {
        elem = document.getElementById("searchResult_" + id);

        if (elem == null) {
            return;
        }
    }

    // Check if the user has already voted on this result
    if (id in currentLiked) {
        // Set thumbs up to green
        elem.firstElementChild.getElementsByClassName("fa-thumbs-up")[0].classList.add("clicked");
    
        // Remove thumbs down color
        elem.firstElementChild.getElementsByClassName("fa-thumbs-down")[0].classList.remove("clicked");
    } else {
        // Remove thumbs up color
        elem.firstElementChild.getElementsByClassName("fa-thumbs-up")[0].classList.remove("clicked");
    }
    
    if (id in currentDisliked) {
        // Set thumbs down to red
        elem.firstElementChild.getElementsByClassName("fa-thumbs-down")[0].classList.add("clicked");
        
        // Remove thumbs up color
        elem.firstElementChild.getElementsByClassName("fa-thumbs-up")[0].classList.remove("clicked");
    } else {
        // Remove thumbs down color
        elem.firstElementChild.getElementsByClassName("fa-thumbs-down")[0].classList.remove("clicked");
    }
}

XHR.registerCallback("removeFeedbackResult", function(data) {
    if (data["success"]) {
        var id = data["id"];

        if (data["feedbackChangeOn"] == "thumbsUp") {
            delete currentLiked[id];
        } else if (data["feedbackChangeOn"] == "thumbsDown") {
            delete currentDisliked[id];
        }

        toggleThumbs(id, null);
    } else {
        console.log("Failed to remove feedback for id: " + data["id"]);
    }
});


// Logout

function logout() {
    XHR.sendPostRequest("logout", {});
}

XHR.registerCallback("logoutResult", function(data) {
    // Redirect to login page

    if (data == true) {
        // Remove local storage
        localStorage.removeItem("username");
        localStorage.removeItem("uuid");
        localStorage.removeItem("auth");

        window.location.href = "/index.html";
    }
});

// createNewResultCard("https://upload.wikimedia.org/wikipedia/commons/6/6c/Star_Wars_Logo.svg", "Star Wars: Episode IV – A New Hope", "https://www.wikidata.org/wiki/Q17738", Q17738);
// createNewResultCard("https://upload.wikimedia.org/wikipedia/commons/0/02/Star_Wars_The_Clone_Wars.png", "Star Wars: The Clone Wars", "https://www.wikidata.org/wiki/Q632672", Q632672);
// createNewResultCard("https://upload.wikimedia.org/wikipedia/commons/4/49/Star_Wars_The_Force_Awakens.jpg", "Star Wars Episode VII: The Force Awakens", "https://www.wikidata.org/wiki/Q6074", Q6074);
// createNewResultCard("https://upload.wikimedia.org/wikipedia/commons/e/ec/Star_Wars_The_Rise_of_Skywalker.png", "Star Wars Episode IX: The Rise of Skywalker", "https://www.wikidata.org/wiki/Q20977110", Q20977110);


// Liked / disliked

function getLikedDisliked() {
    XHR.sendPostRequest("getLikedDisliked", {});
}

XHR.registerCallback("likedDislikedResult", function(data) {
    currentLiked = data["liked"];
    currentDisliked = data["disliked"];

    if (currentTab == "likedTab" || currentTab == "dislikedTab") {
        fillInLikedDisliked();
    }
});

function fillInLikedDisliked(tab) {
    // Clear results list
    clearResultsList();

    // Get list
    if (tab == "likedTab") {
        for (const [key, value] of Object.entries(currentLiked)) {
            createNewResultCard(value["thumbnail"], value["title"], value["link"], key, value["type"]);
        }
    } else if (tab == "dislikedTab") {
        for (const [key, value] of Object.entries(currentDisliked)) {
            createNewResultCard(value["thumbnail"], value["title"], value["link"], key, value["type"]);
        }
    }
}

// Recommendations

function getRecommendations() {
    XHR.sendPostRequest("getRecommendations", {});

    // Show loading screen
    document.getElementById("loadingResults").classList.add("visible");
}

XHR.registerCallback("recommendationsResult", function(data) {
    // Hide loading screen
    document.getElementById("loadingResults").classList.remove("visible");

    // Clear results list
    clearResultsList();

    if (data == null || data.length == 0) {
        document.getElementById("noRecommendationsMessage").hidden = false;
    } else {
        document.getElementById("noRecommendationsMessage").hidden = true;
    }

    // Get list
    for (const [key, value] of Object.entries(data)) {
        createNewResultCard(value["thumbnail"], value["title"], value["link"], value["id"], value["type"]);
    }
});


// Navbar switch

function clearResultsList() {
    // Get parent
    var cards = document.getElementsByClassName("resultCard");

    // Remove all children
    while (cards.length > 0) {
        cards[0].remove();
    }
}

function switchTab() {
    // Get name of tab
    if (this.id == currentTab) return;

    if (this.id == "searchTab") {
        document.getElementById("header").hidden = false;
        fillInSearchResults();
    } else if (this.id == "likedTab" || this.id == "dislikedTab") {
        document.getElementById("header").hidden = true;
        fillInLikedDisliked(this.id);
    } else if (this.id == "recommendationsTab") {
        document.getElementById("header").hidden = true;
        getRecommendations();
    }

    this.disabled = true;

    if (currentTab == "searchTab") {
        document.getElementById("searchTab").disabled = false;
    } else if (currentTab == "likedTab") {
        document.getElementById("likedTab").disabled = false;
    } else if (currentTab == "dislikedTab") {
        document.getElementById("dislikedTab").disabled = false;
    } else if (currentTab == "recommendationsTab") {
        document.getElementById("recommendationsTab").disabled = false;
    }

    document.getElementById("noRecommendationsMessage").hidden = true;
    currentTab = this.id;
}

// Initialization

function init() {
    createNewFilterField(); // First filter field
    getFiltersPOST(); // Get filters from server

    // set callback for all type filters
    Array.from(document.getElementsByClassName("typeSelector")).forEach(button => {
        button.onclick = getFiltersPOST;
    });

    // set search button callback
    document.getElementById("searchButton").onclick = sendSearchPOST;

    // set logout button callback
    document.getElementById("logoutButton").onclick = logout;

    // set tab button callbacks
    document.getElementById("searchTab").onclick = switchTab;
    document.getElementById("likedTab").onclick = switchTab;
    document.getElementById("dislikedTab").onclick = switchTab;
    document.getElementById("recommendationsTab").onclick = switchTab;
}

init();