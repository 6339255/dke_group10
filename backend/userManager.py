from rdflib import Graph, Namespace, Literal, RDF, RDFS, OWL, URIRef, XSD, BNode
from rdflib.serializer import Serializer
import json
from wikidataintegrator import wdi_core
import util

GRAPH_EXPORT_LOCATION = "./graph.rdf"

# Define namespaces
EX = Namespace("http://Example.com/")
FOAF = Namespace("http://xmlns.com/foaf/0.1/")
WD = Namespace("http://www.wikidata.org/entity/")

class UserStore:
    def __init__(self):
        # Load filters from file
        with open("filters.json", "r") as f:
            self.filters = json.load(f)

        # Create a new graph
        self.g = Graph()
        self.initialize_graph()

        # Try to load an existing users graph
        try:
            print("Trying to load existing graph from '" + GRAPH_EXPORT_LOCATION + "'...")
            self.load_graph()
            print("Loaded " + str(len(self.g)) + " triples from '" + GRAPH_EXPORT_LOCATION + "'.")
        except FileNotFoundError:
            print("No '" + GRAPH_EXPORT_LOCATION + "' file found. Creating admin account...")
            self.add_user("admin", "admin", '0')
            self.save_graph()

    ### Graph Initialization ###

    def initialize_graph(self):
        print("Initializing graph...")

        # Bind namespaces to the graph
        self.g.bind("ex", EX)
        self.g.bind("rdf", RDF)
        self.g.bind("rdfs", RDFS)
        self.g.bind("owl", OWL)
        self.g.bind("foaf", FOAF)
        self.g.bind("xsd", XSD)
        self.g.bind("wd", Namespace("http://www.wikidata.org/entity/"))

        # Define User class
        self.g.add((EX.User, RDF.type, OWL.Class))
        self.g.add((EX.User, RDFS.subClassOf, FOAF.Person))

        # Define data properties for user's username, password, uuid and logged in status
        self.g.add((EX.hasUsername, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasUsername, RDFS.domain, EX.User))
        self.g.add((EX.hasUsername, RDFS.range, XSD.string))
        self.g.add((EX.hasPassword, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasPassword, RDFS.domain, EX.User))
        self.g.add((EX.hasPassword, RDFS.range, XSD.string))
        self.g.add((EX.hasUUID, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasUUID, RDFS.domain, EX.User))
        self.g.add((EX.hasUUID, RDFS.range, XSD.string))
        self.g.add((EX.isLoggedIn, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.isLoggedIn, RDFS.domain, EX.User))
        self.g.add((EX.isLoggedIn, RDFS.range, XSD.boolean))

        # Define Media class
        self.g.add((EX.Media, RDF.type, OWL.Class))
        self.g.add((EX.Media, RDFS.subClassOf, OWL.Thing))

        # Define object property for user's preferences
        self.g.add((EX.hasPreference, RDF.type, OWL.ObjectProperty))
        self.g.add((EX.hasPreference, RDFS.domain, EX.User))
        self.g.add((EX.hasPreference, RDFS.range, EX.Media))

        # Define object property for user recommendation
        self.g.add((EX.hasRecommendation, RDF.type, OWL.ObjectProperty))
        self.g.add((EX.hasRecommendation, RDFS.domain, EX.User))
        self.g.add((EX.hasRecommendation, RDFS.range, EX.Media))

        # Define value property for user recommendation
        self.g.add((EX.hasRecommendationValue, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasRecommendationValue, RDFS.domain, EX.hasRecommendation))
        self.g.add((EX.hasRecommendationValue, RDFS.range, XSD.float))

        # Define data property for preference (like/dislike)
        self.g.add((EX.hasPreferenceValue, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasPreferenceValue, RDFS.domain, EX.hasPreference))
        self.g.add((EX.hasPreferenceValue, RDFS.range, XSD.boolean))

        #Define subclasses of Media: Book, Movie, Series, Music
        self.g.add((EX.Book, RDF.type, OWL.Class))
        self.g.add((EX.Book, RDFS.subClassOf, EX.Media))
        self.g.add((EX.Movie, RDF.type, OWL.Class))
        self.g.add((EX.Movie, RDFS.subClassOf, EX.Media))
        self.g.add((EX.Series, RDF.type, OWL.Class))
        self.g.add((EX.Series, RDFS.subClassOf, EX.Media))
        self.g.add((EX.Music, RDF.type, OWL.Class))
        self.g.add((EX.Music, RDFS.subClassOf, EX.Media))

        # Define the Wikidata item class
        self.g.add((WD.Item, RDF.type, OWL.Class))

        # Define subclasses of Media: Book, Movie, Series, Music
        self.g.add((EX.Book, RDFS.subClassOf, WD.Item))
        self.g.add((EX.Movie, RDFS.subClassOf, WD.Item))
        self.g.add((EX.Series, RDFS.subClassOf, WD.Item))
        self.g.add((EX.Music, RDFS.subClassOf, WD.Item))


        # Define data properties for a media object
        # ID
        self.g.add((EX.hasID, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasID, RDFS.domain, EX.Media))
        self.g.add((EX.hasID, RDFS.range, XSD.string))

        # Title
        self.g.add((EX.hasTitle, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasTitle, RDFS.domain, EX.Media))
        self.g.add((EX.hasTitle, RDFS.range, XSD.string))

        # Thumbnail
        self.g.add((EX.hasThumbnail, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasThumbnail, RDFS.domain, EX.Media))
        self.g.add((EX.hasThumbnail, RDFS.range, XSD.string))

        # Link
        self.g.add((EX.hasLink, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasLink, RDFS.domain, EX.Media))
        self.g.add((EX.hasLink, RDFS.range, XSD.string))

        # Type
        self.g.add((EX.hasType, RDF.type, OWL.DatatypeProperty))
        self.g.add((EX.hasType, RDFS.domain, EX.Media))
        self.g.add((EX.hasType, RDFS.range, XSD.string))

        print("Graph initialized.")

    ### I/O ###

    def save_graph(self):
        with open(GRAPH_EXPORT_LOCATION, "wb") as f:
            self.g.serialize(f, format="xml")
    
    def load_graph(self):
        # Load graph from file
        self.g.parse(GRAPH_EXPORT_LOCATION, format="xml")

    ### User Management ###

    def add_user(self, username, password, uuid=None):
        # Clean up username and password
        username = username.strip().lower()
        password = password.strip()

        # Verify that the username doesn't already exist
        if self.user_exists(username):
            return False

        # Create a new resource for the user
        user = BNode()

        # Add the user to the graph
        self.g.add((user, RDF.type, EX.User))
        
        # Generate a new uuid
        if uuid is None:
            uuid = util.genUUID()

        # Add data properties to user instance
        self.g.add((user, EX.hasUsername, Literal(username)))
        self.g.add((user, EX.hasPassword, Literal(password)))
        self.g.add((user, EX.hasUUID, Literal(str(uuid))))
        self.g.add((user, EX.isLoggedIn, Literal("True", datatype=XSD.boolean)))

        # Save graph to file
        self.save_graph()

        return True

    def user_exists(self, username):
        # Clean up username
        username = username.strip().lower()

        query = query = f"""
            SELECT ?username
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        return len(results) != 0

    def get_user(self, username):
        # Clean up username
        username = username.strip().lower()

        query = query = f"""
            SELECT ?username ?password ?uuid ?isLoggedIn
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasPassword ?password .
                ?user ex:hasUUID ?uuid .
                ?user ex:isLoggedIn ?isLoggedIn
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False

        # Create a new user object
        return {
            "username": username,
            "password": list(results)[0]["password"].toPython(),
            "uuid": list(results)[0]["uuid"].toPython(),
            "isLoggedIn": list(results)[0]["isLoggedIn"].toPython()
        }

    def get_user_by_uuid(self, uuid):
        # Clean up username
        username = username.strip().lower()

        query = query = f"""
            SELECT ?username ?password ?uuid ?isLoggedIn
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername ?username .
                ?user ex:hasPassword ?password .
                ?user ex:hasUUID \"{uuid}\" .
                ?user ex:isLoggedIn ?isLoggedIn
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False

        # Create a new user object
        return {
            "username": list(results)[0]["username"].toPython(),
            "password": list(results)[0]["password"].toPython(),
            "uuid": uuid,
            "isLoggedIn": list(results)[0]["isLoggedIn"].toPython()
        }

    def get_user_id(self, username):
        # Clean up username
        username = username.strip().lower()

        query = query = f"""
            SELECT ?username ?uuid
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasUUID ?uuid .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False

        # Create a new user object
        return list(results)[0]["uuid"].toPython()

    def get_user_name(self, uuid):
        query = query = f"""
            SELECT ?username ?uuid
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername ?username .
                ?user ex:hasUUID \"{uuid}\" .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False

        # Create a new user object
        return list(results)[0]["username"].toPython()

    def get_user_password(self, username):
        # Clean up username
        username = username.strip().lower()

        query = query = f"""
            SELECT ?username ?password
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasPassword ?password .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False

        # Create a new user object
        return list(results)[0]["password"].toPython()

    def _get_user_bnode(self, username):
        # Clean up username and password
        username = username.strip().lower()

        # Verify that user exists
        if not self.user_exists(username):
            return False

        query = f"""
            SELECT ?user
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False
        else:
            return list(results)[0][0]

    def is_user_logged_in(self, username):
        # Clean up username
        username = username.strip().lower()

        query = query = f"""
            SELECT ?username ?isLoggedIn
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:isLoggedIn ?isLoggedIn .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(results) == 0):
            return False

        # Create a new user object
        return list(results)[0]["isLoggedIn"].toPython()

    def get_users(self):
        query = query = f"""
            SELECT ?username ?password ?uuid ?isLoggedIn
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername ?username .
                ?user ex:hasPassword ?password .
                ?user ex:hasUUID ?uuid .
                ?user ex:isLoggedIn ?isLoggedIn
            }}
        """

        # Execute the query
        results = self.g.query(query)

        res = []
        for result in results:
            res.append({
                "username": result["username"].toPython(),
                "password": result["password"].toPython(),
                "uuid": result["uuid"].toPython(),
                "isLoggedIn": result["isLoggedIn"].toPython()
            })

        return res
         
    def remove_user(self, username):
        # Clean up username
        username = username.strip().lower()

        # Define the query to retrieve the user by its username
        query = """
            SELECT ?user
            WHERE {
                ?user a EX:User .
                ?user EX:hasUsername "johndoe" .
            }
        """

        # Execute the query
        results = self.g.query(query)

        # Iterate over the results
        for row in results:
            user = row[0]
            # Remove the user from the graph
            self.g.remove((user, None, None))

    def login(self, username, password):
        password = password.strip()
        userBnode = self._get_user_bnode(username)

        if not userBnode:
            return False

        savedPassword = self.get_user_password(username)

        if savedPassword != password:
            return False

        # Update the isLoggedIn property of the user
        self.g.set((userBnode, EX.isLoggedIn, Literal(True)))

        return True

    def logout(self, username):
        userBnode = self._get_user_bnode(username)

        # Set logged out status
        self.g.add((userBnode, EX.isLoggedIn, Literal(False)))

        return True

    ### Preference Management ###

    # Add a preference to a user
    # mediatype: The type of media (e.g. "movie", "music", "book")
    # username: The username of the user
    # wdInstance: The URI of the Wikidata instance
    # preference: The preference value ("True" for like, "False" for dislike)
    def add_preference(self, id, username, preference, properties):
        userBnode = self._get_user_bnode(username)

        # Create a new Media item
        mediaBnode = BNode()
        self.g.add((mediaBnode, RDF.type, EX[properties["type"].capitalize()]))
        self.g.add((mediaBnode, RDFS.subClassOf, EX.Media))
        self.g.add((mediaBnode, RDF.type, WD.Item))
        self.g.add((mediaBnode, EX.hasURI, URIRef(properties["link"])))
        self.g.add((mediaBnode, EX.hasPreferenceValue, Literal(preference)))

        # Add the item properties
        self.g.add((mediaBnode, EX["hasID"], Literal(id)))
        self.g.add((mediaBnode, EX["hasTitle"], Literal(properties["title"])))
        self.g.add((mediaBnode, EX["hasThumbnail"], Literal(properties["thumbnail"])))
        self.g.add((mediaBnode, EX["hasLink"], URIRef(properties["link"])))
        self.g.add((mediaBnode, EX["hasType"], Literal(properties["type"])))

        # Add the Media item as a preference to the given user bnode
        self.g.add((userBnode, EX.hasPreference, mediaBnode))

        # Save the graph
        self.save_graph()

        return True

    # Remove a preference from a user
    def remove_preference(self, username, wdInstance):
        # Define the query to retrieve the user by its username
        query = f"""
            SELECT ?media
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasPreference ?media .
                ?media ex:hasID \"{wdInstance}\" .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        # Iterate over the results
        for row in results:
            media = row[0]
            # Remove the user from the graph
            self.g.remove((media, None, None))

        # Save the graph
        self.save_graph()

        return True

    # Get all preferences for a user
    def get_preferences(self, username):
        # Define the query to retrieve the user by its username
        query = f"""
            SELECT ?media ?preference ?id ?title ?thumbnail ?link ?type
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasPreference ?media .
                ?media ex:hasID ?id .
                ?media ex:hasPreferenceValue ?preference .
                ?media ex:hasTitle ?title .
                ?media ex:hasThumbnail ?thumbnail .
                ?media ex:hasLink ?link .
                ?media ex:hasType ?type .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        # Iterate over the results
        liked = {}
        disliked = {}
        for row in results:
            res = {
                "preference": row["preference"].toPython(),
                "id": row["id"].toPython(),
                "title": row["title"].toPython(),
                "thumbnail": row["thumbnail"].toPython(),
                "link": row["link"].toPython(),
                "type": row["type"].toPython()
            }

            if res["preference"] == True:
                liked[row["id"].toPython()] = res
            else:
                disliked[row["id"].toPython()] = res

        return {
            "liked": liked,
            "disliked": disliked
        }

    ### Recommendations ###

    def add_recommendation(self, id, username, properties, save_graph):
        userBnode = self._get_user_bnode(username)

        # Verify if the user has already rated this item
        # Define the query to retrieve the user by its username
        query = f"""
            SELECT ?media
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasPreference ?media .
                ?media ex:hasID \"{id}\" .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(list(results)) > 0):
            return False

        # Verify that the recommendation is not already in the graph
        # Define the query to retrieve the user by its username
        query = f"""
            SELECT ?media
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasRecommendation ?media .
                ?media ex:hasID \"{id}\" .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        if (len(list(results)) > 0):
            return False

        # Create a new Media item
        mediaBnode = BNode()
        self.g.add((mediaBnode, RDF.type, EX[properties["type"].capitalize()]))
        self.g.add((mediaBnode, RDFS.subClassOf, EX.Media))
        self.g.add((mediaBnode, RDF.type, WD.Item))
        self.g.add((mediaBnode, EX.hasURI, URIRef(properties["link"])))
        self.g.add((mediaBnode, EX.hasRecommendationValue, Literal(properties["score"])))

        # Add the item properties
        self.g.add((mediaBnode, EX["hasID"], Literal(id)))
        self.g.add((mediaBnode, EX["hasTitle"], Literal(properties["title"])))
        self.g.add((mediaBnode, EX["hasThumbnail"], Literal(properties["thumbLink"])))
        self.g.add((mediaBnode, EX["hasLink"], URIRef(properties["link"])))
        self.g.add((mediaBnode, EX["hasType"], Literal(properties["type"])))

        # Add the Media item as a recommendation to the given user bnode
        self.g.add((userBnode, EX.hasRecommendation, mediaBnode))

        # Save the graph
        if save_graph:
            self.save_graph()

        return True

    def get_recommendations(self, username):
        query = f"""
            SELECT ?media ?id ?title ?thumbnail ?link ?type ?score
            WHERE {{
                ?user a ex:User .
                ?user ex:hasUsername \"{username}\" .
                ?user ex:hasRecommendation ?media .
                ?media ex:hasID ?id .
                ?media ex:hasTitle ?title .
                ?media ex:hasThumbnail ?thumbnail .
                ?media ex:hasLink ?link .
                ?media ex:hasType ?type .
                ?media ex:hasRecommendationValue ?score .
            }}
        """

        # Execute the query
        results = self.g.query(query)

        # Iterate over the results
        recommendations = {}
        for row in results:
            res = {
                "id": row["id"].toPython(),
                "title": row["title"].toPython(),
                "thumbnail": row["thumbnail"].toPython(),
                "link": row["link"].toPython(),
                "type": row["type"].toPython(),
                "score": row["score"].toPython()
            }

            recommendations[row["id"].toPython()] = res

        return recommendations