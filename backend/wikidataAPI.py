from wikidataintegrator import wdi_core
import numpy as np 
import threading
from datetime import date

class WikidataAPI:
    def __init__(self):
        self.lastQueryResults = None

    def getResults(self, checkedFilters, userFilters):
        result = {}

        if ("Movies" in checkedFilters):
            if (not (userFilters["director"] == "" and userFilters["title"] == "" and userFilters["genre"] == "" and userFilters["durationMin"] == "" and userFilters["durationMax"] == "" and userFilters["publicationDateStart"] == "" and userFilters["publicationDateEnd"] == "")):
                query = self.movie_QueryBuilder('movie', userFilters["director"], userFilters["title"], userFilters["genre"], userFilters["durationMin"], userFilters["durationMax"], userFilters["publicationDateStart"], userFilters["publicationDateEnd"])
                result.update(self.getQuery(query, parseResults=True))

        if ("Books" in checkedFilters):
            if (not (userFilters["author"] == "" and userFilters["title"] == "" and userFilters["genre"] == "" and userFilters["publicationDateStart"] == "" and userFilters["publicationDateEnd"] == "")):
                query = self.book_QueryBuilder('book', userFilters["author"], userFilters["title"], userFilters["genre"], "", userFilters["publicationDateStart"], userFilters["publicationDateEnd"])
                result.update(self.getQuery(query, parseResults=True))

        if ("Music" in checkedFilters):
            if (not (userFilters["title"] == "" and userFilters["performer"] == "" and userFilters["genre"] == "" and userFilters["durationMin"] == "" and userFilters["durationMax"] == "" and userFilters["publicationDateStart"] == "" and userFilters["publicationDateEnd"] == "")):
                query = self.music_QueryBuilder('music', userFilters["title"], userFilters["performer"], "", userFilters["genre"], userFilters["durationMin"], userFilters["durationMax"], userFilters["publicationDateStart"], userFilters["publicationDateEnd"])
                result.update(self.getQuery(query, parseResults=True))

        return result

    ### Books ###

    def book_QueryBuilder(self, mediatype = 'books',authorfiltername='',title='',genre='',language='',pubrangestart='',pubrangeend='') :
        prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
        stringstart = "SELECT DISTINCT ?{med_type} ?{med_type}Label ?author (group_concat( distinct ?genre ; separator =\"; \" ) as ?genres) ?authorLabel ?image ?title ?languageofwork  WHERE {{".format(med_type = mediatype)
        values_string = "VALUES ?type {{wd:Q7725634 wd:Q571}} ?{med_type} wdt:P31 ?type .  ".format(med_type = mediatype) ;
        if authorfiltername.startswith('http'): # IT IS A URI 
            author_inputstring = authorfiltername.split("; ")
            authorinput_convertedtostatement= ["?{med_type} wdt:P50 wd:{genreURI}".format(med_type = mediatype,genreURI=self._remove_urlwikidata_fromstring(element)) for element in author_inputstring]; 
            values_string_start = "{" #Start union
            values_string_body  = ".  } UNION {".join( authorinput_convertedtostatement) #body of the possible genres
            values_string_end   =".}" # we want another values string, to search by this.
            values_string = values_string_start + values_string_body + values_string_end;  #new values string
        elif authorfiltername !='':  
             author_querystring   = self._find_authors_byname(mediatype='book',authorname=authorfiltername);
             author_wikidataquery = self.getQuery(author_querystring);     
             list_string_authors     = self._ValuesFromWD_Query(author_wikidataquery,itemname='author');
             wdstring_authors        = self._create_WD_string(list_string_authors); 
             values_string = "VALUES ?type {{ {authors} }} ?{med_type} wdt:P50 ?type.   ".format(authors = wdstring_authors,med_type = mediatype); #Change values_string else do nothing maybe add : ?book wdt:P31 wd: .... .         
        imagestring   = "OPTIONAL{{?{med_type} wdt:P18 ?image.}} ".format(med_type = mediatype)
        genrestring   = "?{med_type} wdt:P136 ?genre. ".format(med_type = mediatype)
        if genre.startswith('http'):# input is an URI 
            inputgenres_string = genre.split("; "); #Get individual genres
            inputgenres_convertedtostatement= ["?{med_type} wdt:P136 wd:{genreURI}".format(med_type = mediatype,genreURI=self._remove_urlwikidata_fromstring(element)) for element in inputgenres_string]; 
            values_string_start = "{" #Start union
            values_string_body  = ".  } UNION {".join( inputgenres_convertedtostatement) #body of the possible genres
            values_string_end   =".}" # we want another values string, to search by this.
            values_string = values_string_start + values_string_body + values_string_end;  #new values string
        authorstring  = "?{med_type} wdt:P50 ?author.".format(med_type = mediatype) ;  
        titlestring   = "?{med_type} wdt:P1476 ?title.".format(med_type=mediatype);
        if title != '' :
            titlestring += "FILTER(REGEX(?title, \"{containsname}\", \"i\")) ".format(containsname=title); 
        languagefilter = 'FILTER (langMatches( lang(?title), "EN" ) )'
        languagestring = "?{med_type} wdt:P407 ?languageofwork.".format(med_type=mediatype); 
        endingstring  = "SERVICE wikibase:label {bd:serviceParam wikibase:language \"en\" .} }Group by ?book ?bookLabel ?author ?authorLabel ?image ?title ?languageofwork LIMIT 100"
        pubstring     = self._publicationdate_querybuilder(mediatype,pubrangestart,pubrangeend);
        
        return  prefix+stringstart + values_string +authorstring+ titlestring + genrestring+imagestring+languagefilter+languagestring+pubstring+endingstring;

    def _query_string_limitbyauthor(self, mediatype,wikidataentries): #inputs a list of wikidatastring in form [Q123123] returns string for wikidataquery
        string = "Values ?{med_type}author {{".format(med_type=mediatype); 
        entitieslist = ' '.join([self._create_wd_string(elements) for elements in wikidataentries]);
        end_string = "}} ?{med_type} wdt:P50 ?{med_type}author.".format(med_type=mediatype)
        return string+entitieslist+end_string
        #VALUES ?typeauthor {wd:Q34660}  ?book wdt:P50 ?typeauthor. 

    def _book_querybuilder(self, mediatype = 'book',author='') : 
        stringstart = "SELECT DISTINCT ?{med_type} ?{med_type}Label ?author ?image  WHERE {{".format(med_type = mediatype)
        string_bookselect = "VALUES ?type {{wd:{litwork}}} ?{med_type} wdt:P31 ?type .".format(litwork = 'Q7725634',med_type=mediatype)
        authorstring      = self._author_querybuilder(mediatype,author) 
        imagestring = self._try_image(mediatype); 
        labelstring = "SERVICE wikibase:label {bd:serviceParam wikibase:language \"en\" .} "
        endstring = "}  LIMIT 100"
        return stringstart+string_bookselect+authorstring+imagestring+ labelstring+endstring

    def _find_authors_byname(self, mediatype='book',authorname='') :
        startstring = "SELECT DISTINCT ?author ?authorname WHERE  {"
        isauthorstring = "?author wdt:P106 ?Q36180; wdt:P1559 ?authorname."
        if authorname != '' :
            isauthorstring+= "FILTER(REGEX(?authorname, \"{containsname}\", \"i\"))".format(containsname=authorname)
        endstring = "} LIMIT 1000 "
        return startstring + isauthorstring + endstring



    def _author_querybuilder(self, mediatype,authorname=''): #Creates string and possible filter for director. 
        author_query ="OPTIONAL{{?{med_type} wdt:P50 ?author}}".format(med_type=mediatype) 
        if authorname != '' :
            author_query+= 'FILTER(REGEX(?authorLabel, "{author_filtername}", "i"))'.format(author_filtername=authorname) 
        
        return author_query

    ### Music ###

    def music_QueryBuilder(self,mediatype, title='', performer='',language='' ,genre='', duration_minimum ='', duration_maximum ='', publicationdate_minimum='' ,publicationdate_maximum='' ):
        prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
        string1 = "SELECT DISTINCT ?{med_type} ?title ?performer ?performerLabel (group_concat( distinct ?genre ; separator =\"; \" ) as ?genres) ?languageofwork ?duration ?publicationdate WHERE{{ ".format(med_type=mediatype);
        values_string = "VALUES ?type {Q7302866} ?item wdt:P31 ?type ." #Q7302866 is audio track (can add more options) 
        if performer.startswith('http'): 
            performer_inputstring = performer.split("; ")
            performerinput_convertedtostatement= ["?{med_type} wdt:P175 wd:{genreURI}".format(med_type = mediatype,genreURI=self._remove_urlwikidata_fromstring(element)) for element in performer_inputstring ]; 
            values_string_start = "{" #Start union
            values_string_body  = ".  } UNION {".join(  performerinput_convertedtostatement) #body of the possible genres
            values_string_end   =".}" # we want another values string, to search by this.
            values_string = values_string_start + values_string_body + values_string_end;  #new values string    
        
        if performer != '' : 
            query_for_directors = self._find_creators('music',performer,'performer') 
            values_string = "VALUES ?type {{ {found_creators} }} ?{med_type} wdt:P175 ?type.   ".format(found_creators=query_for_directors,med_type=mediatype);#P175 -> performer change values string
    
        performerstring = "?{med_type} wdt:P175 ?performer.".format(med_type = mediatype) ;  
        languagestring = "OPTIONAL {{?{med_type} wdt:P407 ?languageofwork.}}".format(med_type=mediatype);  #P364 anders dan books!     
        titlestring =  "OPTIONAL {{?{med_type} rdfs:label ?title.}}".format(med_type=mediatype)
        titlefilter = 'FILTER (langMatches( lang(?title), "EN" ) )'
        genrestring   = "?{med_type} wdt:P136 ?genre. ".format(med_type = mediatype)
        if genre.startswith('http'):
            inputgenres_string = genre.split("; "); #Get individual genres
            inputgenres_convertedtostatement= ["?{med_type} wdt:P136 wd:{genreURI}".format(med_type = mediatype,genreURI=self._remove_urlwikidata_fromstring(element)) for element in inputgenres_string]; 
            values_string_start = "{" #Start union
            values_string_body  = ".  } UNION {".join( inputgenres_convertedtostatement) #body of the possible genres
            values_string_end   =".}" # we want another values string, to search by this.
            values_string = values_string_start + values_string_body + values_string_end;  #new values string    
            
        if title != '':
            titlestring += "FILTER(REGEX(?title, \"{containsname}\", \"i\")) ".format(containsname=title);     
        duration_string = self._duration_querybuilder(mediatype, duration_minimum, duration_maximum); 
        publicationdate_string = self._publicationdate_querybuilder(mediatype, publicationdate_minimum, publicationdate_maximum)
        endingstring  = "SERVICE wikibase:label {bd:serviceParam wikibase:language \"en\" .} }"
        endingstring += "GROUP BY ?{med_type} ?title ?performer ?performerLabel ?languageofwork ?duration ?publicationdate  LIMIT 100".format(med_type = mediatype); 
        return prefix+string1 + values_string + performerstring + languagestring + titlestring +titlefilter+genrestring+ duration_string + publicationdate_string + endingstring; 
    
    def _find_performers_byname(self,mediatype = 'music',performername ='') :
        startstring       = "SELECT DISTINCT ?performer ?performername WHERE  {" #
        isperformerstring = "?performer wdt:P106 ?Q639669; wdt:P1559 ?performername."
        if performername != '' : 
            isperformerstring+= "FILTER(REGEX(?performername, \"{containsname}\", \"i\"))".format(containsname=performername); 
            endstring = "} LIMIT 100 "
        return startstring + isperformerstring + endstring; 
    ### Movies ###

    def movie_QueryBuilder(self, mediatype ,director='',title='',genre='',duration_minimum='',duration_maximum='',publicationdate_minimum='',publicationdate_maximum=''):
        prefix = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
        string1      = "SELECT DISTINCT ?{med_type} ?title ?director ?directorLabel  (group_concat( distinct ?genre ; separator =\"; \" ) as ?genres) ?duration ?publicationdate WHERE{{ ".format(med_type=mediatype);
        values_string = "VALUES ?type {wd:Q11424} ?item wdt:P31 ?type ."
        if  director.startswith('http'): # IT IS A URI 
            director_inputstring = director.split("; ")
            directorinput_convertedtostatement= ["?{med_type} wdt:P57 wd:{genreURI}".format(med_type = mediatype,genreURI=self._remove_urlwikidata_fromstring(element)) for element in director_inputstring ]; 
            values_string_start = "{" #Start union
            values_string_body  = ".  } UNION {".join(  directorinput_convertedtostatement) #body of the possible genres
            values_string_end   =".}" # we want another values string, to search by this.
            values_string = values_string_start + values_string_body + values_string_end;  #new values string        
        elif director != '' : #search 
            query_for_directors= self._find_creators(mediatype,director, 'director')
            values_string = "VALUES ?type {{ {found_creators} }} ?{med_type} wdt:P57 ?type.   ".format(found_creators=query_for_directors,med_type=mediatype); #Change values_string else do nothing maybe add : ?book wdt:P31 wd: .... .       
        directorstring = "?{med_type} wdt:P57 ?director.".format(med_type = mediatype) ;  
        languagestring = "OPTIONAL {{?{med_type} wdt:P364 ?languageofwork.}}".format(med_type=mediatype);  #P364 anders dan books! 
        titlestring   = "OPTIONAL {{?{med_type} rdfs:label ?title.}}".format(med_type=mediatype);
        titlefilter = 'FILTER (langMatches( lang(?title), "EN" ) )'
        if title != '':
            titlestring += "FILTER(REGEX(?title, \"{containsname}\", \"i\")) ".format(containsname=title); 
        genrestring   = "?{med_type} wdt:P136 ?genre. ".format(med_type = mediatype)
        if genre.startswith('http'):
            inputgenres_string = genre.split("; "); #Get individual genres
            inputgenres_convertedtostatement= ["?{med_type} wdt:P136 wd:{genreURI}".format(med_type = mediatype,genreURI=self._remove_urlwikidata_fromstring(element)) for element in inputgenres_string]; 
            values_string_start = "{" #Start union
            values_string_body  = ".  } UNION {".join( inputgenres_convertedtostatement) #body of the possible genres
            values_string_end   =".}" # we want another values string, to search by this.
            values_string = values_string_start + values_string_body + values_string_end;  #new values string    
        duration_string = self._duration_querybuilder(mediatype, duration_minimum, duration_maximum); 
        publicationdate_string = self._publicationdate_querybuilder(mediatype, publicationdate_minimum, publicationdate_maximum)
        endingstring  = "SERVICE wikibase:label {bd:serviceParam wikibase:language \"en\" .} }"
        endingstring += "GROUP BY ?{med_type} ?title ?director ?directorLabel ?duration ?publicationdate  LIMIT 100".format(med_type = mediatype); 
        return prefix+string1 + values_string + directorstring + titlefilter + titlestring +   genrestring+ languagestring + duration_string+ publicationdate_string + endingstring; 

    def _director_querybuilder(self, directorname2): #Creates string and possible filter for director. 
        querystringstart = '?item wdt:P57 ?director. '
        directorname     = '?director rdfs:label ?directorname. '
        filterstringdirector = "FILTER(langMatches(lang(?directorname),\"EN\")) "

        if directorname2 == '': #not chosen to pick a director
            dir_querystringend = ''
    
        else: 
            dir_querystringend = 'FILTER(REGEX(?directorname, "{dir_name}", "i"))'.format(dir_name=directorname2) 
        return  querystringstart + directorname + filterstringdirector + dir_querystringend
    
    def _castmember_querybuilder(self, castmemberinput) : #Creates string to get castmembers, not yet working
        castmember_start =  'OPTIONAL {?item wdt:P161 ?castmember.} '
        castmember_name =  'OPTIONAL {?castmember rdfs:label ?castmembername.} '
        castmember_name_namefilter  = 'filter(langMatches(lang(?castmembername),"EN"))'
        if castmemberinput =='':
            castmember_end = '' 
        else: 
            castmember_end = 'FILTER(REGEX(\'?castmember_name, "{castmemberfilter}", "i"))'.format(castmemberfilter =castmemberinput) 
        return castmember_start + castmember_name+ castmember_name_namefilter +castmember_end

    def _find_directors_byname(self, mediatype='movie',authorname='') :
        startstring = "SELECT DISTINCT ?director ?directorname WHERE  {"
        isauthorstring = "?director wdt:P106 ??Q2526255; wdt:P1559 ?directorname."
        if authorname != '' :
            isauthorstring+= "FILTER(REGEX(?directorname, \"{containsname}\", \"i\"))".format(containsname=authorname)
        endstring = "} LIMIT 100 "
        return startstring + isauthorstring + endstring

    def _find_creators(self, mediatypeofcreator,creatorname,whatcreateriscalledinquery): #
        if mediatypeofcreator == 'movie':
            creator_querystring   =  self._find_directors_byname(mediatype=mediatypeofcreator,authorname=creatorname);
        elif mediatypeofcreator == 'music':
            creator_querystring   = self._find_performers_byname(mediatype=mediatypeofcreator,performername=creatorname); 
        creator_wikidataquery =  self.getQuery(creator_querystring);     
        creator_liststring    =  self._ValuesFromWD_Query(creator_wikidataquery,itemname=whatcreateriscalledinquery);
        creator_wdstring      =  self._create_WD_string(creator_liststring);
        print(creator_querystring)
        return creator_wdstring;

    ### Any ###

    def _title_querybuilder(self, title):  #Creates string and possible filter for title
        stringstart = "OPTIONAL { ?item wdt:P1476 ?title.}" 
        if title == '': 
            title_querystringend = ''
        else: 
            title_querystringend = 'FILTER(REGEX(?title, "{title_name}", "i"))'.format(title_name =title) 
        return stringstart + title_querystringend 

    def _genre_querybuilder(self, genreinput) : #Creates string and filter for genre
        genre_stringstart = 'OPTIONAL {?item wdt:P136 ?genre.} '
        genre_name        = 'OPTIONAL {?genre rdfs:label ?genrename.} '
        genre_namefilter  = 'filter(langMatches(lang(?genrename),"EN"))'
        if genreinput == '': 
            genre_stringend = ''
        else: 
            genre_stringend = 'FILTER(REGEX(\'?genre_name, "{genrefilter}", "i"))'.format(genrefilter = genreinput) 
        return genre_stringstart + genre_name +genre_namefilter + genre_stringend

    def _duration_querybuilder(self, mediatype, durationmin, durationmax) : #Creates string to get the duration
        duration_start = 'OPTIONAL {{?{med_type} wdt:P2047 ?duration.}} '.format(med_type = mediatype)
        duration_filterstring = '' 
        if durationmin != '' : 
            duration_filterstring = 'FILTER({duration_filtermin} <= ?duration && ?duration < {duration_filtermax}).'.format(duration_filtermin=durationmin,duration_filtermax=durationmax); #duration in minutes
        return duration_start +duration_filterstring; 

    def _publicationdate_querybuilder(self, mediatype, startrangeinput,endrangeinput) : #Creates string with possible filter for publication date and if there is a range given filters. 
        publicationdate_start = 'OPTIONAL {{?{med_type}  wdt:P577 ?publicationdate.}} '.format(med_type=mediatype)
        if startrangeinput == '' : 
            publication_filterstring= '' 
        else: 
            publication_filterstring= 'FILTER("{filterstart}" ^^xsd:dateTime <= ?publicationdate && ?publicationdate < "{filterend}"^^xsd:dateTime).'.format(filterstart= startrangeinput,filterend = endrangeinput)
        return publicationdate_start +publication_filterstring

    ### Utils ###

    def getQuery(self, query, endpoint='https://query.wikidata.org/sparql', parseResults=False):
        print("Querying...")
        res = wdi_core.WDItemEngine.execute_sparql_query(query, prefix=None, endpoint=endpoint, user_agent=None, as_dataframe=False, max_retries=1000, retry_after=60)
        if parseResults:
            res = self.convertToDict(res)
            self.lastQueryResults = res
        return res

    def _get_wdentityfromdic(self, inputdictionary,binding): # Gets the wikidata entity from a dictionary
        return inputdictionary[binding]; 

    def _create_valuelist(self, bindingdictionary,binding) :
        return [self._get_wdentityfromdic(inputdictionary,binding) for inputdictionary in bindingdictionary]                         

    def _remove_urlwikidata_fromstring(self, inputstring):
        return inputstring.replace('http://www.wikidata.org/entity/','')

    def _create_wd_string(self, string): #creates a wd:Q.... string from a Q... entity. 
        return "wd:"+string

    def _try_image(self, itemname): #Tries to retrieve image from wikidata item 
        image_query =  "OPTIONAL{{?{med_type} wdt:P18 ?image.}} ".format(med_type=itemname)
        return image_query

    def _ValuesFromWD_Query(self, inputdictionary,itemname='author') : #Get the Q_items from a wikidata query, itemname is the name you binded the value to in the sparQL query. 
        firstlayer_results    = inputdictionary["results"]; 
        firstlayer_list       = firstlayer_results["bindings"]; #Get the list of results (if length 0 no results I think 
        secondlayer_results   = self._create_valuelist(firstlayer_list,itemname) 
        secondlayer_url_value = self._create_valuelist(secondlayer_results,'value') # Get the actual value 
        wd_itemlist           = [self._remove_urlwikidata_fromstring(elements) for elements in secondlayer_url_value]; # Remove the url part
        return wd_itemlist #IF no results returns an empty list. 

    def _create_WD_string(self, entities): #Inputs a list of entities and creates a list of wd:Q... items 
        stringlist = ["wd:" + element for element in entities]; 
        return ' '.join(stringlist);

    # Returns the last query results
    def getLastQueryResults(self):
        return self.lastQueryResults

    # Returns the result for the given id from the last query
    def getQueryResult(self, id):
        if self.lastQueryResults and id in self.lastQueryResults:
            return self.lastQueryResults[id]
        return None

    # Converts the given query results to a dictionary
    def convertToDict(self, queryResults):
        result = {}
        for item in queryResults["results"]["bindings"]:
            resItem = {}
            for key in item:
                resItem[key] = item[key]["value"]

            thumbLink = None

            if ("thumbnail" in resItem):
                thumbLink = resItem["thumbnail"]

            if ("image" in resItem):
                thumbLink = resItem["image"]

            if ("book" in resItem):
                id = self._remove_urlwikidata_fromstring(resItem["book"])
                type = "book"
                if (thumbLink == None):
                    thumbLink = "https://thesecondangle.com/wp-content/uploads/2022/02/p08j8hmv-1.jpg"
                resItem["link"] = resItem["book"]
            elif ("movie" in resItem):
                id = self._remove_urlwikidata_fromstring(resItem["movie"])
                type = "movie"
                if (thumbLink == None):
                    thumbLink = "https://www.filmconvert.com/Content/images/headers/film-stocks.jpg"
                resItem["link"] = resItem["movie"]
            elif ("music" in resItem):
                id = self._remove_urlwikidata_fromstring(resItem["music"])
                type = "music"
                if (thumbLink == None):
                    thumbLink = "https://joy1.videvo.net/videvo_files/video/free/2016-10/thumbnails/161021_04_CoffeeShop_RecordPlayer_1080p_small.jpg"
                resItem["link"] = resItem["music"]
            else:
                continue
            resItem["type"] = type
            resItem["id"] = id
            resItem["thumbLink"] = thumbLink

            result[id] = resItem
        return result
    
    ### Recommend queries 
    def _find_more_by_creator(self, initialresult): #creates query string to find more media made by a person. 
        if "author" in initialresult:
            creator = initialresult["author"]
        elif "director" in initialresult:
            creator = initialresult["director"]
        elif "performer" in initialresult:
            creator = initialresult["performer"]
        elif "composer" in initialresult:
            creator = initialresult["composer"]

        if ";" in creator:
            joined_creatorlist_splittedagain = creator.split('; ') 
            listreduced = joined_creatorlist_splittedagain[0:5] 
            creator = '; '.join(listreduced);

        resDict = {}
        querymovies = self.movie_QueryBuilder('movie',director= creator)
        resDict.update(self.getQuery(querymovies, parseResults=True)) #more results

        querymusic     = self.music_QueryBuilder('music', performer= creator)
        resDict.update(self.getQuery(querymusic, parseResults=True)) #more results

        querybooks     = self.book_QueryBuilder('book', authorfiltername= creator)
        resDict.update(self.getQuery(querybooks, parseResults=True)) #more results

        return resDict
    
    def _find_more_by_genre(self,initialresult) :# input what kind of mediatype and what type of initial results
        if "genres" not in initialresult:
            return {}

        if ";" in initialresult["genres"]:
            joined_genrelist_splittedagain = initialresult["genres"].split('; ') 
            listreduced = joined_genrelist_splittedagain[0:5] 
            res = '; '.join(listreduced);
        else:
            res = initialresult["genres"];

        resDict = {}    
        querymovies = self.movie_QueryBuilder('movie',genre= res)
        resDict.update(self.getQuery(querymovies, parseResults=True)) #more results

        querymusic     = self.music_QueryBuilder('music', genre= res) 
        resDict.update(self.getQuery(querymusic, parseResults=True)) #more results

        querybooks    = self.book_QueryBuilder(mediatype = 'book', genre=  res) 
        resDict.update(self.getQuery(querybooks, parseResults=True)) #more results
        
        return resDict

    def _recommendations_finder(self, uuid, item, callback):

        res=self._find_more_by_creator(item)
        res.update(self._find_more_by_genre(item))

        callback(uuid, res)

    def recommendation_runner(self, uuid, item, callback):
        t = threading.Thread(target=self._recommendations_finder, args=(uuid, item, callback))
        t.start()
        
            
        
        
        
        
        
        
        

def connectionTest():
    api = WikidataAPI()
    query = '''
    SELECT DISTINCT ?item ?itemLabel WHERE {
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
        {
            SELECT DISTINCT ?item WHERE {
                {
                    ?item p:P31 ?statement0.
                    ?statement0 (ps:P31/(wdt:P279*)) wd:Q2431196.
                }
                UNION
                {
                    ?item p:P31 ?statement1.
                    ?statement1 (ps:P31/(wdt:P279*)) wd:Q47461344.
                }
                UNION
                {
                    ?item p:P31 ?statement2.
                    ?statement2 (ps:P31/(wdt:P279*)) wd:Q2188189.
                }
            }
            LIMIT 100
        }
    }
    '''

    result = api.getQuery(query)
    print(result)

def substring_after(s, delim):#splits string at delimeter
        return s.partition(delim)[2]

def queryTest():
    api = WikidataAPI()
   
    query2 = api.book_QueryBuilder('book',authorfiltername='stephen king'); 
    print(query2) 
    result2 = api.getQuery(query2, parseResults=True) 
    print(result2) 
    abc = api._distancefunction(result2[0], result2[2]); 
    print(str(abc)+ 'distance'); 



if __name__ == "__main__":
    #connectionTest()
    queryTest()
