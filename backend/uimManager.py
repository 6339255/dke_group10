import numpy as np
import json
from datetime import date

EXPORT_LOCATION = "./uim.json"

class uimManager():
    def __init__(self):
        self.keys = []
        self.items = []
        self.users = []
        self.userItemMatrix = np.zeros((0, 0))

        try:
            self.importFromFile()
        except:
            print("Failed to import userItemMatrix from " + EXPORT_LOCATION + ". Creating new one.")
            self.addUser("0")
            self.exportToFile()

    def addUser(self, uuid):
        self.users.append(uuid)
        self.userItemMatrix = np.append(self.userItemMatrix, np.zeros((1, len(self.keys))), axis=0)
        self.exportToFile()

    def removeUser(self, uuid):
        index = self.users.index(uuid)
        self.users.pop(index)
        self.userItemMatrix = np.delete(self.userItemMatrix, index, axis=0)
        self.exportToFile()

    def addItem(self, id, item):
        if id in self.keys or item is None:
            return
        self.keys.append(id)
        self.items.append(item)
        self.userItemMatrix = np.append(self.userItemMatrix, np.zeros((len(self.users), 1)), axis=1)
        self.exportToFile()

    def removeItem(self, itemID):
        if not itemID in self.keys:
            return
        index = self.keys.index(itemID)
        self.keys.pop(index)
        self.items.pop(index)
        self.userItemMatrix = np.delete(self.userItemMatrix, index, axis=1)
        self.exportToFile()

    def addPreference(self, uuid, itemID, preference):
        if not uuid in self.users:
            return False
        userIndex = self.users.index(uuid)

        if not itemID in self.keys:
            return False
        itemIndex = self.keys.index(itemID)

        if preference == True:
            preference = 1
        elif preference == False:
            preference = -1
        
        self.userItemMatrix[userIndex][itemIndex] = preference

        self.exportToFile()
        return True

    def removePreference(self, uuid, itemID):
        if not uuid in self.users:
            return
        userIndex = self.users.index(uuid)

        if not itemID in self.keys:
            return
        itemIndex = self.keys.index(itemID)

        self.userItemMatrix[userIndex][itemIndex] = 0
        self.exportToFile()

    def getPreference(self, uuid, itemID):
        userIndex = self.users.index(uuid)
        itemIndex = self.keys.index(itemID)
        return self.userItemMatrix[userIndex][itemIndex]

    def getPreferences(self, uuid):
        userIndex = self.users.index(uuid)
        return self.userItemMatrix[userIndex]

    def getPreferencedItems(self, uuid):
        prefs = self.getPreferences(uuid)

        items = []
        for i in range(len(prefs)):
            if prefs[i] != 0:
                items.append((self.items[i], prefs[i]))
        
        return items

    def getPreferencesForItem(self, itemID):
        itemIndex = self.keys.index(itemID)
        return self.userItemMatrix[:, itemIndex]

    def export(self):
        return {
            "users": self.users,
            "keys": self.keys,
            "items": self.items,
            "userItemMatrix": self.userItemMatrix.tolist()
        }

    def exportToFile(self, location=EXPORT_LOCATION):
        with open(location, "w") as f:
            f.write(json.dumps(self.export()))

    def importFromFile(self, location=EXPORT_LOCATION):
        with open(location, "r") as f:
            data = json.loads(f.read())
            self.users = data["users"]
            self.keys = data["keys"]
            self.items = data["items"]
            self.userItemMatrix = np.array(data["userItemMatrix"])

    def _search_dictionary_for_creator(self,item): # inputs an item as dictionary and returns it creator (author,director or performer) 
        if "author" in item: 
            creator = item[ 'author'] 
        elif 'performer' in item: 
            creator = item['performer'] 
        elif 'director' in item : 
            creator = item['director']
        else: #NOT A GIVEN CREATOR IN WIKDIATA
            creator = ''
        return creator
    def _search_dictionary_for_genres(self,item) :
        if 'genres' in item: 
            genres = item['genres'] ;
        else: 
            genres = '' ;
        return genres 
    def _search_dictionary_for_pubdate(self,item): 
        if 'publicationdate' in item: 
            pubdate = item['publicationdate'] ; 
        else: 
            pubdate = '' 
        return pubdate
    
    def _get_datetime(self,pubdate): #returns date element from pubdate in form yyyy-mm-dd
        year = int(pubdate[0:4]);         
        month = int(pubdate[5:7]); 
        day = int(pubdate[8:10]); 
        return date(year,month,day);
    def _intersection(self,lst1, lst2): #function intersection2 lists 
            return list(set(lst1) & set(lst2))
        
    def _calc_difference_dates(self,pubdate1,pubdate2) :
        if pubdate1=='' or pubdate2 =='': 
            return 0 #NOT SIMILAR AT ALL 
        else: # no empty pubdate
            date_one = self._get_datetime(pubdate1)
            date_two = self._get_datetime(pubdate2) 
            delta    = date_one - date_two 
            difference_days = delta.days; 
            years_apart = difference_days / 365;
            coefficientperyear = 0.1;
            similarity = min(0, 1-coefficientperyear*years_apart)
        return similarity ; 
    
    def _calc_difference_genre(self,genrelist1,genrelist2):
        lijst1 = genrelist1.split('; ') # splits at seperator
        lijst2 = genrelist2.split('; ') 
        if len(lijst1)==0 or len(lijst2)==0: 
               similarity = 0 
        else: # not 2 empty genrelists
        
            sizelist1 = len(lijst1); 
            sizelist2 = len(lijst2); 
            intersectionlist = self._intersection(lijst1, lijst2); 
            sizelist_intersection = len(intersectionlist); 
            similarity = sizelist_intersection**2 / (sizelist1*sizelist2) #Tried to make cosine similarity
        return similarity; 
    
    def _calc_difference_creator(self,creator1,creator2): 
        if creator1 == '' or creator2 == '' : 
            similarity = 0.1 #maybe likes unknown stuff
        else:
            if creator1 == creator2 : 
                similarity =1 ;
            else : 
                similarity = 0 ; 
        return similarity; 
    
    def _distancefunction(self,item1,item2, pref) :# input 2 wikidata items, calculate how similar they are. 
        creator_item1 = self._search_dictionary_for_creator(item1); 
        creator_item2 = self._search_dictionary_for_creator(item2);
        genres_item1 = self._search_dictionary_for_genres(item1)
        genres_item2 = self._search_dictionary_for_genres(item2)
        publicationdate_item1 = self._search_dictionary_for_pubdate(item1); 
        publicationdate_item2 = self._search_dictionary_for_pubdate(item2);
        similarity_pubdate = self._calc_difference_dates(publicationdate_item1, publicationdate_item2)
        similarity_genre   = self._calc_difference_genre(genres_item1,genres_item2) ;
        similarity_creator = self._calc_difference_creator(creator_item1,creator_item2);

        score = (similarity_pubdate + similarity_genre + similarity_creator)/3;
        if (pref < 0):
            return 1 - score
        else:
            return score

    def getItemScore(self, item, uimItems):
        if "score" in item:
            return item["score"]

        score = 0
        for (i, pref) in uimItems:
            d = self._distancefunction(item, i, pref)
            if d > score:
                score = d
        
        return score

    def getAllItemScores(self, uuid, items):
        uimItems = self.getPreferencedItems(uuid)

        for item in items:
            item["score"] = self.getItemScore(item, uimItems)
        
        return items

    def sortItems(self, items, uuid, minScore=0):
        uimItems = self.getPreferencedItems(uuid)

        # Filter by score
        items = list(filter(lambda x: self.getItemScore(x, uimItems) >= minScore, items))

        return sorted(items, key=lambda x: self.getItemScore(x, uimItems), reverse=True)
