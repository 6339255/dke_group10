####################################################################################################
# This file contains the main backend code for the project. It is responsible for handling requests
# from the frontend and communicating with the wikidataAPI and userStore modules.
####################################################################################################

from flask import Flask, request
import flask
import json
from flask_cors import CORS
import wikidataAPI as wapi
import userManager
import uimManager

DEBUG_FLAG = False
MIN_RECOMMENDATION_SCORE = 0.3

app = Flask(__name__)
CORS(app)
wapi = wapi.WikidataAPI()
userStore = userManager.UserStore()
userItemMatrix = uimManager.uimManager()

# Hello World!
@app.route("/")
def hello():
    return "Hello, World!"

# Register a new user in the userStore and add them to the userItemMatrix
@app.route('/register', methods=["POST"])
def register():
    if request.method != "POST":
        print("Invalid request method on /register")

    received_data = request.get_json()
    username = received_data["data"]['username']
    password = received_data["data"]['password']

    res = userStore.add_user(username, password);
    uuid = userStore.get_user_id(username)

    if(res):
        userItemMatrix.addUser(uuid)
        print("User registered: " + username)
    else:
        print("User failed to register: " + username)

    response = {
        "type": "registerResult",
        "data": {
            "success": res,
            "uuid": uuid,
            "username": username
        }
    }

    return flask.Response(status=200, response=json.dumps(response))

# Login a user in the userStore
@app.route('/login', methods=["POST"])
def login():
    if request.method != "POST":
        print("Invalid request method on /login")

    received_data = request.get_json()
    username = received_data["data"]['username']
    password = received_data["data"]['password']

    res = userStore.login(username, password);

    if (res):
        print("User logged in: " + username)
    else:
        print("User failed to log in: " + username)

    response = {
        "type": "loginResult",
        "data": {
            "success": res,
            "uuid": userStore.get_user_id(username),
            "username": username
        }
    }

    return flask.Response(status=200, response=json.dumps(response))

# Logout a user in the userStore
@app.route('/logout', methods=["POST"])
def logout():
    if request.method != "POST":
        print("Invalid request method on /logout")

    received_data = request.get_json()
    username = received_data['username']

    res = userStore.logout(username);

    if (res):
        print("User logged out: " + username)
    else:
        print("User failed to log out: " + username)

    response = {
        "type": "logoutResult",
        "data": res
    }

    return flask.Response(status=200, response=json.dumps(response))

# Return the list of filters given the chosen media types
@app.route('/filters', methods=["POST"])
def getFilters():
    if request.method != "POST":
        print("Invalid request method on /filters")

    # Load filters from file
    with open("filters.json", "r") as f:
        data = json.load(f)

    received_data = request.get_json()
    message = received_data["data"]['checkedFilters']

    allFilters = set()
    for type in message:
        allFilters.update(data[type])

    response = {
        "type": "filters",
        "data": list(allFilters)
    }

    return flask.Response(status=200, response=json.dumps(response))

# Return the list of results given the chosen filters and user preferences
@app.route('/search', methods=["POST"])
def getSearch():
    if request.method != "POST":
        print("Invalid request method on /search")

    received_data = request.get_json()
    checkedFilters = received_data["data"]['checkedFilters']
    userFilters = received_data["data"]['userFilters']

    #print(checkedFilters)
    #print(userFilters)

    results = wapi.getResults(checkedFilters, userFilters)

    if (results != {}):
        results = userItemMatrix.sortItems(results.values(), received_data["uuid"])

        resDict = {}
        for result in results:
            resDict[result["id"]] = result
    else:
        resDict = {}


    response = {
        "type": "searchResults",
        "data": {
            "success": (results != {}),
            "results": resDict
        }
    }

    return flask.Response(status=200, response=json.dumps(response))

# Scores and stores potential recommendations for a user
def recommendations_handler(uuid, items):
    if (items == {}):
        return

    results = userItemMatrix.getAllItemScores(uuid, items.values())
    username = userStore.get_user_name(uuid)

    if (username == False):
        return

    for result in results:
        userStore.add_recommendation(result["id"], username, result, False)

    userStore.save_graph()

# Handle a user's feedback on a result (thumbs up or thumbs down)
def handleFeedback(request, preference):
    if request.method != "POST":
        if (preference == True):
            print("Invalid request method on /thumbsUp")
        else:
            print("Invalid request method on /thumbsDown")

    received_data = request.get_json()
    id = received_data["data"]["id"]

    result = wapi.getQueryResult(id)
    userItemMatrix.addItem(id, result)

    if (received_data["data"]["firstUnset"]):
        userStore.remove_preference(received_data["username"], id)
        userItemMatrix.removePreference(received_data["uuid"], id)

    userStore.add_preference(id, received_data["username"], preference, received_data["data"]["properties"])
    userItemMatrix.addPreference(received_data["uuid"], id, preference)
    if (result != None):
        wapi.recommendation_runner(received_data["uuid"], result, recommendations_handler)

    response = {
        "type": "feedbackResult",
        "data": {
            "success": True,
            "id": id,
            "preference": preference,
            "properties": received_data["data"]["properties"]
        }
    }

    return flask.Response(status=200, response=json.dumps(response))

# Handle a user's thumbs up on a result
@app.route('/thumbsUp', methods=["POST"])
def thumbsUp():
    return handleFeedback(request, True)

# Handle a user's thumbs down on a result
@app.route('/thumbsDown', methods=["POST"])
def thumbsDown():
    return handleFeedback(request, False)

# Handle a user's removal of a thumbs up or thumbs down
def removeFeedback(request, type):
    if request.method != "POST":
        print("Invalid request method on /removeThumbsUp")

    received_data = request.get_json()
    id = received_data["data"]["id"]
    username = received_data["username"]

    if (username == False):
        return flask.Response(status=200, response=json.dumps({"type": "removeFeedbackResult", "data": {"success": False}}))
  
    res = userStore.remove_preference(username, id)
    userItemMatrix.removePreference(received_data["uuid"], id)

    response = {
        "type": "removeFeedbackResult",
        "data": {
            "success": res,
            "id": id,
            "feedbackChangeOn": type
        }
    }

    return flask.Response(status=200, response=json.dumps(response))

# Handle a user's removal of a thumbs up
@app.route('/removeThumbsUp', methods=["POST"])
def removeThumbsUp():
    return removeFeedback(request, "thumbsUp")

# Handle a user's removal of a thumbs down
@app.route('/removeThumbsDown', methods=["POST"])
def removeThumbsDown():
    return removeFeedback(request, "thumbsDown")

# Return the list of liked and disliked results for a user
@app.route('/getLikedDisliked', methods=["POST"])
def getLikedDisliked():
    if request.method != "POST":
        print("Invalid request method on /getLikedDisliked")

    received_data = request.get_json()
    username = received_data["username"]

    res = userStore.get_preferences(username)

    response = {
        "type": "likedDislikedResult",
        "data": res
    }

    return flask.Response(status=200, response=json.dumps(response))

@app.route('/getRecommendations', methods=["POST"])
def getRecommendations():
    if request.method != "POST":
        print("Invalid request method on /getRecommendations")

    received_data = request.get_json()
    username = received_data["username"]

    res = userStore.get_recommendations(username)
    res = userItemMatrix.sortItems(res.values(), received_data["uuid"], MIN_RECOMMENDATION_SCORE)

    response = {
        "type": "recommendationsResult",
        "data": res
    }

    return flask.Response(status=200, response=json.dumps(response))


if __name__ == "__main__":
    app.run("127.0.0.1", 8000, debug=DEBUG_FLAG)