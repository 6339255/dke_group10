# Content-based recommender system

This project is loosely based on the following boilerplate: [Python backend with JavaScript frontend: how to](https://tms-dev-blog.com/python-backend-with-javascript-frontend-how-to/)

## Requirements

 - [Python](https://www.python.org/downloads/)
 - [Node JS](https://nodejs.org/en/download/)

## Set up

Install the requirements for the backend by first creating a virtual environment in the backend directory:

```
cd backend
python -m venv venv
```
Then install the dependencies:

```
pip install -r "requirements.txt"
```

In another terminal, run the following:
```
cd frontend
npm install
```

## Running the project

Start the backend server using the following command in the first terminal:

```
python app.py
```

Start the frontend in the second terminal:

```
npx http-server -p 8001
```

Navigate to the address the frontend is hosted at. This is `http://localhost:8001` by default.

## Debugging with VSCode

To debug the backend access the debug tab, select `Python: Flask` and click the green play button. Breakpoints should then work. The frontend needs to be started as described above and can be debugged in the browser.
